import { FunctionComponent } from "react";
import Signal from "../images/signal.svg";
import Pool from "../images/pool.svg";
import Parking from "../images/parking.svg";
import { Rating } from "@material-ui/lab";
import Tag from "../components/tags/tag";
import Image from "../components/image/image";
import Title from "../components/text/title";
import Description from "../components/text/description";
import Icon from "../components/icon/icon";
import Button from "../components/buttons/button";

type Images = {
  id: string,
  url: string
}

type Price = {
  total: number
}

type DataProps = {
  id: string,
  name: string,
  address: string,
  stars: number,
  image?: Array<Images>,
  rates: Array<Price>
};

const Card: FunctionComponent<DataProps> = ({ name, address, stars, image, rates, id }) => {

  const styles = {
    red: {
      color: "#D0021B",
      backgroundColor: "rgba(208, 2, 27, 0.1)",
      marginRight: 17
    },
    blue: {
      color: "#4A81E2",
      backgroundColor: "rgba(74, 129, 226, 0.15)",
      marginRight: 17
    },
    offer: {
       color: "#FFFFFF",
       backgroundColor: "#579B08",
    },
    rating: {
      fontSize: 11, 
      marginRight: 15
    }
  };
   console.log("rates", rates)
  return (
    <div className="container" key={id}>
      <div className="item">
        <div className="img">
            {image?.slice(0,1).map(e => <Image img={e.url} alt={e.id} /> )}
        </div>
        <div className="items">
        <div className="content-container">
          <div className="content-text-and-icon">
            <div className="text-container">
              <Title
                text={name}
              />
              <Description text={address} />
            </div>

            <div className="Icons-Container">
              <Rating
                style={styles.rating}
                name="simple-controlled"
                value={stars}
              />
              <Icon img={Signal} alt={"signal"} />
              <Icon img={Pool} alt={"pool"} />
              <Icon img={Parking} alt={"parking"} />
            </div>
          </div>

          <div className="tag-container">
            <Tag text={"Recomendado"} styles={styles.blue} />
            <Tag text={"No reembolsable"} styles={styles.red} />
          </div>
        </div>
        <div className="Description-and-price">
        <div>
        <div className="title-and-offer">
        <Title text={"Total final"} />
        <Tag text={"10% OFF"} styles={styles.offer} />
        </div>
        <Description text={"6 noches, 2 personas"} />
        <div className="total-price">
        <p className="info-title">ARS 2.300</p>
        </div>
        <Description text={"Impuestos y tasas incluidos"} />
        </div>
        
        <Button text={"Ver Hotel"}/>
        </div>
    </div>
      </div>
    </div>
  );
};

export default Card;
