import { FETCH_INIT, FETCH_SUCCESS, FETCH_FAILURE } from "../constants";

export const InitialState = {
    isLoading: true,
    isError: false,
}


type State = {
  data?: HNResponse;
  isLoading: boolean;
  isError?: boolean;
 }

 
 type HNResponse = {
  clusters: {
  id: string;
  name: string;
  address: string;
  rates: {
    total: number;
  }[],
  stars: number;
  images: {
    id: string,
    url: string
  }[];
  }[]
 };

type Action =
  | { type: 'FETCH_INIT'; }
  | { type: 'FETCH_SUCCESS'; payload: HNResponse }
  | { type: 'FETCH_FAILURE'; payload: string }

export const Reducer = (state: State , action: Action): State => {
  switch (action.type) {
    case FETCH_INIT:
      return {
        isLoading: true,
        isError: false
      };
    case FETCH_SUCCESS:
      return {
        isLoading: false,
        isError: false,
        data: action.payload
      };
    case FETCH_FAILURE:
      return {
        isLoading: false,
        isError: true,
      }
    default:
      return state;
  }
};

