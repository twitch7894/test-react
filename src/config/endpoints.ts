/**
 * ENDPOINTS
 */

const HOST = "https://basset.free.beeceptor.com/reactjs-test";

export const ENDPOINTS = {
      ACCOMMODATIONS: `${HOST}/accommodations`
}