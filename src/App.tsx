import { useCallback, useEffect, useReducer } from "react"; 
import { InitialState, Reducer } from "./reducers";
import axios from "axios";
import { FETCH_INIT, FETCH_SUCCESS, FETCH_FAILURE } from "./constants";
import { ENDPOINTS } from "./config/endpoints";
import Card from "./containers/card";


function App() {
  const [state, dispatch] = useReducer(Reducer, InitialState);

  type Rate = {
    total: number
  }

  const getDataAccommodations = useCallback(async (url: string) => {
        dispatch({ type: FETCH_INIT })
       try {
          await axios(url).then(result => dispatch({ type: FETCH_SUCCESS, payload: result.data }));
          
       } catch(err) {
        dispatch({ type: FETCH_FAILURE, payload: err })
       }
  }, [dispatch])

  const ResultRates = (value: Array<Rate>) => {
        return value.map(e => e)
  }

  useEffect(() => {
      getDataAccommodations(ENDPOINTS.ACCOMMODATIONS);
  }, [getDataAccommodations]);

  return (
    <div className="App">
      {!state.isLoading && (
        <div className="">
          {state.data?.clusters.map(e => (
            <Card 
            id={e.id}
            name={e.name}
            address={e.address}
            stars={e.stars}
            image={e.images}
            rates={ResultRates(e.rates)}
            />
          ))}
        </div>
      )}
       {state.isError && (
         <p>Error 505</p>
       )}
    </div>
  );
}

export default App;
