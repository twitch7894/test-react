import { FunctionComponent } from "react";

type ImageProps = {
    img: string,
    alt: string
}

const Image:FunctionComponent<ImageProps> = ({ img, alt }) =>  <img className="image" src={img} alt={alt} />;
    
export default Image