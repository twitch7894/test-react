import { FunctionComponent } from "react";

type TagProps = {
    text: String
    styles: Object
}

const Tag:FunctionComponent<TagProps> = ({ text, styles }) =>  <p className="tag" style={styles}>{text}</p>;
    
export default Tag