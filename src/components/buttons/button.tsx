import { FunctionComponent } from "react";

type ButtonProps = {
    text: String
}

const Button:FunctionComponent<ButtonProps> = ({ text }) =>  <button className="brand_primary">{text}</button>;
    
export default Button