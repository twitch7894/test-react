import { FunctionComponent } from "react";

type IconsProps = {
    img: string,
    alt: string
}

const Icon:FunctionComponent<IconsProps> = ({ img, alt }) =>  <img className={"type-icon"} src={img}  alt={alt} />;
    
export default Icon