import { FunctionComponent } from "react";

type TitleProps = {
    text: string
}

const Title:FunctionComponent<TitleProps> = ({ text }) =>  <p className="info-title">{text}</p>;
    
export default Title