import { FunctionComponent } from "react";

type DescriptionProps = {
    text: String
}

const Title:FunctionComponent<DescriptionProps> = ({ text }) =>  <p className="info-description">{text}</p>;
    
export default Title